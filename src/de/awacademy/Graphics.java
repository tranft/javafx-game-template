package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Graphics {

    // Deklarierung der Eigenschaften
    private GraphicsContext gc;
    private Model model;

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }

    // Löschen des Spielerbalkens
    public void draw() {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

        // Zeichnen der vertikalen Spielfeldbegrenzungslinie
        gc.setStroke(Color.rgb(128, 128, 128, 1));
        gc.strokeLine(1152, 0, 1152, 800);

        // Zeichnen der horizontalen Spielfeldbegrenzungslinie
        gc.setStroke(Color.rgb(128, 128, 128, 1));
        gc.strokeLine(1152, 400, 1536, 400);

        // Zeichnen des Spielerbalkens
        gc.setFill(Color.GRAY);
        gc.fillRoundRect(model.getPlayer().getX(), model.getPlayer().getY(),
                model.getPlayer().getW(), model.getPlayer().getH(), model.getPlayer().getXX(), model.getPlayer().getYY());

        // Zeichnen des Balls
        gc.setFill(Color.GREY);
        gc.fillOval(model.getBall().getX(), model.getBall().getY(), model.getBall().getRx(), model.getBall().getRy());

        // Zeichnen der Bricks
        for (int i = 0; i < model.getBricks().length; i++) {
            if (model.getShowBrick()[i] == true) {
                gc.fillRect(model.getBricks()[i].x, model.getBricks()[i].y, model.getBricks()[i].w, model.getBricks()[i].h);
            }
        }

        // Zeichnen des Scores
        gc.setFill(Color.GREY);
        gc.setFont(Font.font("Helvetica", FontWeight.BOLD,100));
        gc.fillText("Score: \n" + model.getScore(), 1200,175);

        // Zeichnen des Ergebnisses
        gc.setFill(Color.GREY);
        gc.setFont(Font.font("Helvetica", FontWeight.BOLD,100));
        gc.fillText("State: \n" + model.getState(), 1200,500);
    }
}