package de.awacademy;

import de.awacademy.model.Model;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MainGame extends Application {

    private Model model;

    @Override
    public void start(Stage stage) throws Exception {

        Model model = new Model();
        Canvas canvas = new Canvas(model.WIDTH, model.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);

        // Aufruf der Scene und Setzen der Hintergrundfarbe
        Scene scene = new Scene(group, Color.BLACK);

        stage.setScene(scene);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(gc, model);
        InputHandler inputHandler = new InputHandler(model);
        scene.setOnKeyPressed(
                event -> inputHandler.keyPressed(event)
        );
        scene.setOnKeyReleased(event -> {
            model.getPlayer().setPadright(false);
            model.getPlayer().setPadleft(false);
        });

        Timer timer = new Timer(graphics, model);
        timer.start();

        stage.show();
    }
}