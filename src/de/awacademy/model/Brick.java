package de.awacademy.model;

public class Brick {

    // Deklarierung der Bricks-Objekt-Variablen
    public int x;
    public int y;
    public int w;
    public int h;

    // Bricks-Konstruktor
    public Brick(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    // Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }
}
