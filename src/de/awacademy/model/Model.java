package de.awacademy.model;

import javafx.geometry.Rectangle2D;

public class Model {

    // Größe der Szene
    public final double WIDTH = 1536;
    public final double HEIGHT = 800;

    // Deklarierung
    private Player player;
    private Ball ball;
    private Brick brick;
    private Brick[] bricks;
    private boolean[] showBrick;
    private int score;
    private String state;

    // Start-Positionen von Spielerbalken und Ball
    public Model() {
        this.player = new Player(516, 768);
        this.ball = new Ball(0, 0);

        // Start-Positionen der Bricks
        ////////////////////////////////////////
        //
        // Schablone für 2-Dimensionales Array:
        //
        // 00100000100
        // 00010001000
        // 00111111100
        // 01101110110
        // 11111111111
        // 10111111101
        // 10100000101
        // 00011011000
        //
        ////////////////////////////////////////

        this.bricks = new Brick[96]; // Brick-Menge
        this.showBrick = new boolean[96]; // Brick-Menge
        this.score = 0;
        this.state = "";

        int xx = 272; // Startposition des 1. Bricks Horizontal
        int yy = 51; // Startposition des 1. Bricks Vertikal
        int count = 1;

        for (int i = 0; i < bricks.length; i++) {
            bricks[i] = new Brick(xx, yy, 50, 50); // Brick-Größe
            showBrick[i] = true; // Brick-Sichtbarkeit
            xx += 51; // Brick-Abstand Horizontal
            if (count % 12 == 0) { // Bricks pro Zeile
                xx = 272; // Startposition weiterer Brick-Zeilen Horizontal
                yy += 51; // Brick-Abstand Vertikal
            }
            count++;
        }
    }

    // Ball-Kollisionsverhalten mit Spielfeldbegrenzung
    public void collisions() {
        if (ball.getX() < 0 || ball.getX() + ball.getBallCollider() > 1152) // < Spielfeldbegrenzung links/rechts
            ball.setDx(ball.getDx() * -1); // Holt sich Wert von Dx und schreibt ihn in Dx neu
        if (ball.getY() < 0) // < Spielfeldbegrenzung oben
            ball.setDy(ball.getDy() * -1);
        if (ball.getY() + ball.getBallCollider() > 800) // < Spielfeldbegrenzung unten
            setState(1);

        // Ball-Kollisionsverhalten mit Spielerbalken
        if (ball.getX() + ball.getBallCollider() > player.getX() && ball.getX() < player.getX() + player.getPaddleCollider() && ball.getY() + ball.getBallCollider() > player.getY() && ball.getY() < player.getY() + player.getH()) {
            ball.setDy(ball.getDy() * -1);
        }
        // Ball-Kollisionsverhalten mit Bricks MC
        Rectangle2D ballRect = new Rectangle2D(ball.getX(), ball.getY(), ball.getBallCollider(), ball.getBallCollider());
        for (int i = 0; i < bricks.length; i++) {
            Rectangle2D BrickRect = new Rectangle2D(bricks[i].getX(), bricks[i].getY(), bricks[i].getW(), bricks[i].getH());
            if (BrickRect.intersects(ballRect) && showBrick[i] == true) {
                showBrick[i] = false;
                ball.setDy(ball.getDy() * -1);
                score++;
                if (score == 96) { // Siegbedingung
                    setState(2);
                }
            }
        }
    }

    // Timed-Methoden
    public void update(long nowNano) {
        this.getBall().moveBall(state);
        this.collisions();
        if (getBall().getY() + getBall().getBallCollider() > 800) {
            //setState(1);
            //stop();
        }
        // Bewegung Spielerbalken
        if (getPlayer().isPadright() == true && getPlayer().getX() + getPlayer().getPaddleCollider() < 1146) {
            getPlayer().move(15, 0);
        }
        if (getPlayer().isPadleft() == true && getPlayer().getX() > 6) {
            getPlayer().move(-15, 0);
        }
    }

    // Getter
    public Player getPlayer() {
        return player;
    }

    public Ball getBall() {
        return ball;
    }

    public double getWIDTH() {
        return WIDTH;
    }

    public Brick[] getBricks() {
        return bricks;
    }

    public boolean[] getShowBrick() {
        return showBrick;
    }

    public int getScore() {
        return score;
    }

    public String getState() {
        return state;
    }

    public void setState(int gameOver) {
        if (gameOver == 1) {
            state = "GAME \nOVER!";
        }
        if (gameOver == 2)
            state = " YOU \nWON!";
    }
}