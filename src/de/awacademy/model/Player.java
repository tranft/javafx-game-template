package de.awacademy.model;

public class Player {

    // Deklarierung der Spielerbalken-Variablen
    private int x;
    private int y;
    private int h;
    private int w;
    private int rx;
    private int ry;
    private boolean padright;
    private boolean padleft;
    private int paddleCollider;

    // Player-Konstruktor
    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 25;
        this.w = 150;
        // Balken-Kantenrundungen
        this.rx = 25;
        this.ry = 25;
        // Balken-Bewegungen und Kollider
        this.padright = false;
        this.padleft = false;
        this.paddleCollider = w;
    }

    // Bewegung des Spielerbalkens
    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    // Getter-Methoden
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public int getH() {
        return h;
    }
    public int getW() {
        return w;
    }
    public int getXX() {
        return rx;
    }
    public int getYY() {
        return ry;
    }

    public boolean isPadright() {
        return padright;
    }

    public boolean isPadleft() {
        return padleft;
    }


    public int getPaddleCollider() {
        return paddleCollider;
    }

    public void setPadright(boolean padright) {
        this.padright = padright;
    }

    public void setPadleft(boolean padleft) {
        this.padleft = padleft;
    }
}