package de.awacademy.model;

public class Ball {

    // Deklarierung der Ball-Objekt-Variablen
    private int x;
    private int y;
    private int rx;
    private int ry;
    // Deklarierung der Ball-Bewegungsvariablen
    private int dx;
    private int dy;
    private int ballCollider;

    // Ball-Konstruktor
    public Ball(int x, int y) {
        this.x = 500;
        this.y = 600;
        this.rx = 24;
        this.ry = rx;
        this.dx = 10;
        this.dy = 10;
        this.ballCollider = rx;
    }

    // Methode zum Bewegen des Balls
    public void moveBall(String state) {
        if(!state.equals(" YOU \nWON!") && !state.equals("GAME \nOVER!")) {
            x += dx;
            y += dy;
        }
    }

    // Getter-Methoden
    public int getX() { return x; }
    public int getY() { return y; }
    public int getRx() { return rx; }
    public int getRy() { return ry; }
    public int getDx() { return dx; }
    public int getDy() { return dy; }
    public int getBallCollider() { return ballCollider; }

    // Setter
    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }
}
