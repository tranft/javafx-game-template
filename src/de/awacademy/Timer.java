package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    //
    private Graphics graphics;
    private Model model;

    // Konstruktoren
    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    // Methodenaufrufe im Millisekundentakt
    @Override
    public void handle(long nowNano) {
        model.update(nowNano); // Aufruf der eigentlichen Timed-Methoden
        graphics.draw();
    }
}