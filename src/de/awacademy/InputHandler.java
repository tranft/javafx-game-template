package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyEvent;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    // Steuerung des Spielerbalkens
    boolean padRight;
    boolean padLeft;

    public void keyReleased(KeyEvent e) {
        int key = Integer.parseInt(e.getCode().toString());
        padRight = false;
        padLeft = false;
    }

    public void keyPressed(KeyEvent e) {
        if (e.getCode().toString().equals("RIGHT")) {
            model.getPlayer().setPadright(true);
        }
        if (e.getCode().toString().equals("LEFT")) {
            model.getPlayer().setPadleft(true);
        }
    }
}